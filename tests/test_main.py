"""
A simple test as an example
"""
import math

from lunarxdashboard.main import calculate_orbit_velocity


def test_calculate_orbit_velocity():
    # Test with known values for Earth
    planet_mass = 5.97219e24  # kg
    radius = 6371e3  # meters

    # compare expected value with actual value using math.isclose
    expected_velocity = 7.905363939663303e3  # m/s
    actual_velocity = calculate_orbit_velocity(planet_mass, radius)
    assert math.isclose(expected_velocity, actual_velocity, rel_tol=1e-3)

    # Test with zero values
    assert calculate_orbit_velocity(0, 100) == 0
