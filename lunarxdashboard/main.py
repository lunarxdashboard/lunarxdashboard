"""
A simple function to show functionality.
"""


def calculate_orbit_velocity(planet_mass, radius):
    """Calculate the orbital velocity of an object around a planet.

    Args:
        planet_mass (float): mass of the planet in kg
        radius (float): radius of the planet in meters

    Returns:
        float: orbital velocity in m/s
    """
    g = 6.67430e-11  # gravitational constant
    return (g * planet_mass / radius) ** 0.5


def main():
    """Calculate the orbital velocity of an object around a planet."""
    m_earth = 5.9721e24  # kg
    r_earth = 6371  # km
    altitude = int(input("Input of your space asset altitude: "))
    print(
        "Your orbital velocity: ",
        calculate_orbit_velocity(planet_mass=m_earth, radius=r_earth + altitude),
    )


if __name__ == "__main__":
    main()
