# lunarXdashboard


The Lunar Exploration Dashboard gives an overview of past, ongoing and future moon missions.
Interesting information about what their missions was/is/will be, including location and scientific discoveries.

For more information, please open in issue or get in touch: ceglarek@fsr.tu-darmstadt.de
